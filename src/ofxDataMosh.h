/*
 *  ofxDataMosh.h
 *
 *  Created by Bernardo Rodrigues <bernardoar@protonmail.com>
 *  Copyright 2018 Silicon Arts. Released under MIT License.
 *
 */

#pragma once

#include <stdio.h>
#include "ofMain.h"

class ofxDataMosh {

    public:
        ofxDataMosh();
        ofxDataMosh(int repeatPframes);

        ~ofxDataMosh();

    private:
        int repeatPframes;
};
