/*
 *  ofxDataMosh.h
 *
 *  Created by Bernardo Rodrigues <bernardoar@protonmail.com>
 *  Copyright 2018 Silicon Arts. Released under MIT License.
 *
 */

#include "ofxDataMosh.h"

ofxDataMosh::ofxDataMosh(){
}

ofxDataMosh::ofxDataMosh(int repeatPframes){
    this->repeatPframes = repeatPframes;

    printf("%d%s\n", this->repeatPframes, " test from constructor");
}

ofxDataMosh::~ofxDataMosh(){
    //clear();
}
