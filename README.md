ofxDataMosh
=====================================

Introduction
------------
I could only find one single datamoshing tool that is able to run on Linux: https://github.com/happyhorseskull/you-can-datamosh-on-linux

It is basically a Python script that wraps around the famous ffmpeg, a vast software tool suite of libraries and programs for handling video, audio, and other multimedia files and streams. It also has the algorithm to edit i-frames and p-frames.

What’s datamoshing?
------------
![alt text](https://.png "Logo Title Text 1")

In plain English, datamoshing is an umbrella term for an array of cool effects you get by glitching, altering, or otherwise breaking the fundamental structure of video files’ data. We datamosh because we can, and because it looks really cool.

What's happening in the Python datamosh tool is that first the video file is converted to AVI format which is glitch friendly as it sort-of doesn't care if you delete frames from the middle willy-nilly (mp4 gets real mad if you delete stuff in a video file).

There are 2 types of frames that we're dealing with: i-frames and p-frames. I-frames (aka key frames) give a full frame's worth of information while p-frames are  used to calculate the difference from frame to frame and avoid storing lots of redundant frame information. A video can be entirely i-frames but the file size is much larger than setting an i-frame every 10 or 20 frames and making the rest p-frames. The first i-frame is the only one that's required and after that we use p-frames to calculate from frame to frame. The encoding algorithm then makes inter-frame calculations and sometimes interesting effects happen.                                                       

Initially datamoshing was just deleting the extra i-frames maybe smooshing some p-frames in from another video and seeing what you got. However the glitchers eventually grew bored of this and discovered if they repeated p-frames that the calculations would cause a blooming effect and the results were real rowdy. So that's what the repeat_p_frames variable does and that's why "it sounds like a dying printer"-@ksheely. Because we're repeating p-frames the video length may get much longer. At ((25fps - 1 i-frame)) * 15 or (24 * 15) a single second of 24 frames turns into 360 frames which is (360 frames / 25 fps) = 14.4 seconds.

After we're done mucking around with i-frames and p-frames the results are fed to ffmpeg which locks in the glitches and makes a twitter-ready video.

https://en.wikipedia.org/wiki/Video_compression_picture_types

License
-------
ofxDataMosh is distributed under the MIT License.

Installation
------------
Clone this repository into your `openFrameworks/addons/` folder.

Dependencies
------------

 - [ofxAvCodec](https://github.com/kritzikratzi/ofxAvCodec) ??

Compatibility
------------
oF v0.10.1

Known issues
------------
For the moment ofxDataMosh only reads an input video file and writes the datamoshed video file. We plan future support for ofVideoPlayer.

Version history
------------

### Version 0.1 (Date):
- copy from oF Addon template
- add Python scripts inside `you-can-datamosh-on-linux`
